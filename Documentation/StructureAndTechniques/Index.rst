﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Structure and techniques
========================

For the general structure we'll follow the standard for modern
(extbase) extensions. Features and possibilities from TYPO3 CMS 6.2LTS
will be used. Both pages and content elements will be based on Fluid
templates. This means that a lot of conventions can be used instead of
configuration.


