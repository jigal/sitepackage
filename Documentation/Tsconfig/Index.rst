﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


TSConfig
========

The Page TSConfig which is normally stored in a field in the root page
can be moved to the file  **Configuration/TSConfig/Page.ts** .

In order to load this a line needs to be added to  **ext\_tables.php**
:

.. code-block:: php

   \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
           '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TSConfig/Page.ts">'
   );

Although the actual TSConfig could be included in  **ext\_tables.php**
it's of course easier for maintenance to have it in a separate file.

In a similar way it's possible to load user TSConfig (
:php:`addUserTSConfig()` ).


