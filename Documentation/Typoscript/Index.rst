﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


TypoScript
==========

Another item that is usually stored in records is TypoScript. Since
TYPO3 CMS 6.2 it's possible to include an entire directory (plus
subdirectories) with a single include command. The files in this
directory will be sorted (files first, then directories). The easiest
way to use this features is to use a naming scheme such as:
:code:`<nn><description>.ts` :

.. code-block:: none

 |-- 01-Configuration.ts
 |-- 10-PageTemplate.ts
 |-- 11-PageMetadata.ts
 |-- 50-MetaData.ts
 |-- 51-MainMenu.ts
 `-- ....

The actual include statements will be added to
**Configuration/TypoScript/Static/setup.txt** :

.. code-block:: none

   <INCLUDE_TYPOSCRIPT: source="DIR:EXT:typo3coder/Configuration/TypoScript/Elements" extensions="ts">
   <INCLUDE_TYPOSCRIPT: source="DIR:EXT:typo3coder/Configuration/TypoScript/Extensions" extensions="ts">

The TypoScript still needs to be registered by adding to
**ext\_tables.php** :

.. code-block:: php

   \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
           $_EXTKEY,
           'Configuration/TypoScript/Static',
           'typo3coder'
   );

To load the TypoScript you need to include the static templates, just
as with any other extension.


.. toctree::
   :maxdepth: 5
   :titlesonly:
   :glob:

   WhatToStoreInTheExtension/Index

