﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


What to store in the extension?
-------------------------------

For TypoScript we have to distinguish between configuration that is
specific for a particular server (assuming that we have at least a
development installation and a live website) and what is common for
the website. It could very well be that we need to set the
absRefPrefix and that it's different for the development installation
compared to the live site. Then we'll store that in a constant in a
TypoScript template record. In the .ts files we'll use this constant
as usual. In the end we can simply install the same sitepackage
extension on both installations and still have the correct setting.


