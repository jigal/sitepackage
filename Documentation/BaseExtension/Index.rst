﻿.. include:: Images.txt

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Base extension
==============

|img-1|

In the example an extension with the key “ *typo3coder* ” is
created. The basic structure is quite similar to those of extbase
extensions.

The Classes section will contain the PHP helper classes which are
needed. These include viewhelpers and the backend layout provider.

The Configuration section contains everything that is regarded as
configuration. These include flexforms, realUrl configuration, TC,
Tsconfig and TypoScript.

The Resources section is split in two parts, Private and Public.
Everything that is not shown directly will have a place in the Private
section; backend layouts, JavaScript (in most configurations the
JavaScript files are concatenated and/or compressed, so the single JS
files are not directly sent to the browser), Language files, SASS (or
LESS, etcetera) and templates.

The Public section contains the items which are directly sent to the
browser. We'll use a separate sub-section for Backend-related items.

In the root we'll have the usual files  **ext\_emconf.php,
ext\_icon.gif, ext\_localconf.php** and **ext\_tables.php** .


