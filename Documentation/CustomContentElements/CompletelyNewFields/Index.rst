﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Completely new fields
---------------------

If the existing fields are not enough for the content element and
storing the extra fields in a flexform is not the right solution (the
fields need to be indexed for example) then the solution would be to
add extra columns to the tt\_content table.

This example is not present in this demonstration sitepackage, but
since the previous examples already explained the necessary changes in
TCA for a new content element it shouldn't be very hard to implement.

The new tt\_content fields need to be included in a
**ext\_tables.sql** file and the basic field configuration needs to be
added to the  *'columns'* section. By looking at the tt\_content TCA
file in the core there is enough inspiration; further details can be
found in the TCA documentation.


