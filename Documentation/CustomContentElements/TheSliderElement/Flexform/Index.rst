﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Flexform
^^^^^^^^

A major point of criticism on TemplaVoilà was that the actual content
was stored in XML structures (belonging to the flexform that defines
the form). It was almost impossible to use this in queries and it was
not clean enough for many.

For data which doesn't need to be queries there is not much against a
flexform. There are loads of possibilities to create forms and one of
them is using repeated sections.

The structure of a flexform is documented and most of the
possibilities can be found in the TCA documentation. The flexform for
the slider is stored in
**Configuration/FlexForms/flexform\_slider.xml** .

The content from a flexform is stored in the field 'pi\_flexform' in
the tt\_content table. It needs to be added to the configuration for
this field:

.. code-block:: php

   $GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][',slider'] =
           'FILE:EXT:typo3coder/Configuration/Flexforms/flexform_slider.xml';

After the ['ds'] is a comma separated value. The first part is the
list\_type for which the flexform should be used and the second part
the CType. If one of the parts is empty or a '\*' then it matches any
value. In this case we only care about the CType which should be
'slider'.

