﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Registering the content element
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is done like with the 'intro' element:

.. code-block:: php

   $GLOBALS['TCA']['tt_content']['types']['slider'] = array(
     'showitem' => '--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
                    section_frame;LLL:EXT:cms/locallang_ttc.xlf:section_frame_formlabel,
                    pi_flexform; ;,
                  --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,
                    --palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.visibility;visibility,
                    --palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.access;access,
                  --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.extended'
   );

Next add the element to the dropdown list:

.. code-block:: php

   $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array(
     'LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_slider_title',
     'slider',
     'EXT:typo3coder/Resources/Public/Backend/Images/slider_small.png'
   );

