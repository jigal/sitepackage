﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Frontend rendering
^^^^^^^^^^^^^^^^^^

Just as simple as the intro element:

::

   tt_content.slider = FLUIDTEMPLATE
   tt_content.slider {
     template = FILE
     template.file = EXT:typo3coder/Resources/Private/Templates/ContentELements/Slider.html
     layoutRootPath = EXT:typo3coder/Resources/Private/Templates/Layout/
   }

In the Fluid template we run into another challenge: how to get the
data from the flexform. This is solved with a small viewhelper in
**Classes/ViewHelpers/FlexFormViewHelper.php** .With this viewhelper
the Fluid template becomes really simple and clean:

.. code-block:: xml

   {namespace j=jigal\typo3coder\ViewHelpers}
   
   <j:flexForm>
     <div class="flexslider">
       <ul class="slides">
         <f:for each="{flexform.sGeneral.images}" as="sliderImage">
           <li>
             <j:typolink parameter="{sliderImage.url}">
               <f:image src="{sliderImage.image}" treatIdAsReference="1"/>
             </j:typolink>
           </li>
         </f:for>
       </ul>
     </div>
   </j:flexForm>

