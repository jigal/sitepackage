﻿.. include:: Images.txt

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


The 'slider' element
--------------------

|img-3|

The image slider starts with the same *'general'* and
*'frame'* palettes. Next it will get a bit more challenging.

The editor can add images and links to the element, but they need to
be able to include as many images as they want.

This feature is well known from TemplaVoilà and with a bit of effort
it can be done with just core functionality.


.. toctree::
   :maxdepth: 5
   :titlesonly:
   :glob:

   Flexform/Index
   RegisteringTheContentElement/Index
   FrontendRendering/Index

