﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Re-using existing fields
------------------------

In most cases the tt\_content table (where content elements are
stored) contains enough fields to build a content element from. In the
core file  **typo3/sysext/frontend/Configuration/TCA/tt\_content.php**
all the fields of tt\_content are configured. Some of the fields are
for internal use by TYPO3 CMS, but many are meant for specific content
element types and can easily be used by our custom content element.


