﻿.. include:: Images.txt

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Some extra fun!
---------------

As a bonus there is some configuration for the top-left logo in the
backend, the logo above the backend login form and extra CSS for the
login form (which is far from perfect, but enough for a demonstration.

.. code-block:: php

   $relativePath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY);
   $GLOBALS['TBE_STYLES']['logo_login'] = $relativePath . 'Resources/Public/Backend/Images/LogoLogin.png';
   $GLOBALS['TBE_STYLES']['logo'] = $relativePath . 'Resources/Public/Backend/Images/typo3-topbar@2x.png';
   $GLOBALS['TBE_STYLES']['inDocStyles_TBEstyle'] .= '
   #t3-login-form:after {
     content: "\00a0";
     background-image: url("' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Backend/Images/peeking.png");
     width: 220px;
     height: 187px;
     display: inline-block;
     overflow: hidden;
     position: relative;
     left: 201px;
     bottom: 533px;
   }
   ';

|img-5|


