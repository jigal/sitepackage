﻿.. include:: Images.txt

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


New content element wizard
--------------------------

Most editors use the  *'New content element wizard'* to add a new
content element to a page. This can be configured in page TSConfig.
Because we need to add some generated paths the configuration is added
to  **ext\_localconf.php** instead of a separate file.

.. code-block:: php

   \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
     'mod {
        wizards.newContentElement.wizardItems.extra {
          header = LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra
          elements {
            intro {
              icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Backend/Images/intro.png
              title = LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_intro_title
              description = LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_intro_description
              tt_content_defValues {
                CType = intro
              }
            }
            slider {
              icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Backend/Images/slider.png
              title = LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_slider_title
              description = LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_slider_description
              tt_content_defValues {
                CType = slider
              }
            }
          }
          show = *
        }
      }'
   );

There is now a new tab 'extra' with two elements 'intro' and 'slider'.
Each has a title, description and icon.

|img-4|


