﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


History
^^^^^^^

In really old versions of TYPO3 CMS there were no tabs in the backend
forms. There were section which were indicated by the :code:`--div--`
separator in the TCA. Later on an option was introduced
('dividers2tabs') to transform the dividers into a tabbed screen.

Palettes were a feature to have extra (less important) fields in a
group which could be made visible with the click on an icon.

