﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Backend configuration
---------------------

The backend forms are defined in the  *'types'* section. Here we see
the familiar items such as  *'header'* ,  *'text'* ,  *'textpic'*
(text with images). The  *'showitem'* definition contains the parts
which are displayed.


.. toctree::
   :maxdepth: 5
   :titlesonly:
   :glob:

   History/Index
   CurrentSituation/Index

