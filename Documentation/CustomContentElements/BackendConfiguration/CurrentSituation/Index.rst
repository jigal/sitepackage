﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Current situation
^^^^^^^^^^^^^^^^^

With TYPO3 4.5 the backend forms were reconfigured. Items that
belonged together were grouped and the styling was more uniform.

Divs are now turned into tabs by default and the palettes in the core
are forced to be expanded all the time. Palettes are now used only to
group fields.

