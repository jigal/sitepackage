﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Default content and pages?
--------------------------

It is possible to include a .t3d export file in an extension. This
will be imported when the extension is installed. Such an extension is
then called a distribution (see the Introduction Package in the
Extension Manager).

The big drawback is that the .t3d export will only be imported at the
first installation of the distribution. If you update it and install
the updated extension the export file will not be imported anymore.
This is done to prevent the page structure and content from appearing
multiple times in an installation.

It would be a nice idea for a site kick starter, but since one of the
goals of this sitepackage is to make it easier to update an existing
site in a single operation this wouldn't really work.


