﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


A new palette
^^^^^^^^^^^^^

For the slightly different header fields there is enough inspiration
in the original tt\_content TCA. Look in the  *'palettes'* section for
the palettes that are defined for the core elements.

.. code-block:: php

   $GLOBALS['TCA']['tt_content']['palettes']['introheader'] = array(
     'showitem' => 'header;LLL:EXT:cms/locallang_ttc.xlf:header_formlabel,
            --linebreak--, subheader;LLL:EXT:cms/locallang_ttc.xlf:subheader_formlabel,
            --linebreak--, header_link;LLL:EXT:cms/locallang_ttc.xlf:header_link_formlabel',
     'canNotCollapse' => 1
   );

It adds the  *'header'* field with a label from the core locallang
file, a line break, the  *'subheader'* field, and so on.

This palette can be used in the definition of the content element:

.. code-block:: php

   $GLOBALS['TCA']['tt_content']['types']['intro'] = array(
     'showitem' => '--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
                    section_frame;LLL:EXT:cms/locallang_ttc.xlf:section_frame_formlabel,
                    --palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.header;introheader,
                    bodytext;Text;;richtext:rte_transform[flag=rte_enabled|mode=ts_css],
                    rte_enabled;LLL:EXT:cms/locallang_ttc.xlf:rte_enabled_formlabel,
                  --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,
                    --palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.visibility;visibility,
                    --palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.access;access,
                  --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.extended'
   );

It starts with the palette  *'general'* , next there is the field
*'section\_frame'* , the new palette *'introheader',* and so on.

Finally the new content element has to be added to the list of
available content elements (the select box in the general section of a
content element):

.. code-block:: php

   $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array(
     'LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_intro_title',
     'intro',
     'EXT:typo3coder/Resources/Public/Backend/Images/intro_small.png'
   );

This defines a label from a language file, the internal name of the
content element ( *'intro'* ) which was already used in the *'types'*
list earlier, and an icon.

