﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Frontend rendering
^^^^^^^^^^^^^^^^^^

Now that the backend knows about a new type the frontend needs to be
able to render it too. All the definitions are in the
:code:`tt_content` section:

::

   tt_content.intro = FLUIDTEMPLATE
   tt_content.intro {
     template = FILE
     template.file = EXT:typo3coder/Resources/Private/Templates/ContentELements/Intro.html
     layoutRootPath = EXT:typo3coder/Resources/Private/Templates/Layout/
   }

In the Fluid template there was a problem with the rendering of the
link (displayed as “Read more”). In the backend there is a field which
can be filled with the wizard. This field can contain a page UID, a
file, an external URL, a mail address,... plus the settings for
target, title and CSS class. Fluid currently doesn't have a viewhelper
that will take all these into consideration. The page link viewhelper
can accept these parameters, but will not use target, title and class.

In **Classes/ViewHelpers/TypolinkViewHelper.php** is a viewhelper to
render the link completely with all these attributes.

