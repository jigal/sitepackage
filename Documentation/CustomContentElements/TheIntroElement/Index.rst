﻿.. include:: Images.txt

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


The 'intro' element
-------------------

The content element should have these fields:

|img-2|

On top there is the usual  *'general'* block.

Next there is the option to set a frame (which we need in this
template to set the width of a content element in the grid).

For the header we don't need all the fields of a normal  *'header'* or
*'headers'* palette, so we'll have to make our own palette.

The last part of the  *'General'* tab is an RTE field.

On the  *'Access'* tab are the usual fields to determine visibility
for frontend user and other related fields.

In the definition is also the beginning of the configuration of a tab
*'Extended'* . This will be added in case other extensions add fields
to all TCA types. These will be added at the end of the field list and
will thus end up in the  *'Extended'* tab. Tabs without fields are
hidden by default, so the tab  *'Extended'* is only visible if an
extension adds fields this way.

A new feature in TYPO3 CMS 6.2 is that we can simply change the TCA by
putting files in :code:`Configuration/TCA/Overrides/` . The TCA in
these files will be added to the cached TCA for extra speed. By
convention we'll simply use as filename :code:`<tableName>.php` ; in
our case: :code:`Configuration/TCA/Overrides/tt_content.php` .


.. toctree::
   :maxdepth: 5
   :titlesonly:
   :glob:

   ANewPalette/Index
   FrontendRendering/Index

