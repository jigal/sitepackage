﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Custom content elements
=======================

Most projects need custom content elements besides the ones available
from the core. These could be a list of links, an image slider, an
element for an introduction text, etcetera. The core already provides
enough functionality for this. Compared to the possibilities supplied
by TemplaVoilà the only thing missing is an area inside a content
element where an editor can put other content elements. In many cases
this can be solved for a design by supplying extra page templates, but
if this functionality is needed the extension “gridelements” can
provide it.


.. toctree::
   :maxdepth: 5
   :titlesonly:
   :glob:

   Re-usingExistingFields/Index
   BackendConfiguration/Index
   TheIntroElement/Index
   TheSliderElement/Index
   NewContentElementWizard/Index
   CompletelyNewFields/Index
   DefaultContentAndPages/Index
   SomeExtraFun/Index

