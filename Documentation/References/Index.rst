﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


References
==========

- TCA documentation
  http://docs.typo3.org/typo3cms/TCAReference/Index.html

- backend layout file provider by Georg Ringer
  https://github.com/georgringer/belayout\_fileprovider

- New content element wizard http://docs.typo3.org/typo3cms/TSconfigRefe
  rence/PageTsconfig/Mod/Index.html#new-content-element-wizard-mod-
  wizards-newcontentelement

- Gridelements
  http://typo3.org/extensions/repository/view/gridelements


