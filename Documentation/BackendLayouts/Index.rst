﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Backend layouts
===============

For some time it's possible to define the layout of the various
content areas in the Page module using so called Backend Layouts.
These are usually stored in records (which an easy wizard to create
them), but this makes it harder to migrate them between installations.

In TYPO3 CMS 6.2 a feature was introduced that allows extensions to
have a so called dataprovider for backend layouts.

Georg Ringer made an example
`https://github.com/georgringer/belayout\_fileprovider
<https://github.com/georgringer/belayout_fileprovider>`_ for such an
extension. In this sitepackage is a very simplified version which
assumes that the files are in a specific location.

The class is located in  **Classes/Provider/FileProvider.php** . The
provider needs to be registerd with:

.. code-block:: php

   $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['BackendLayoutDataProvider']['typo3coder'] = 'jigal\\typo3coder\\Provider\\FileProvider';

The last part between square brackets contains the prefix that will be
used in the backend layout identifier. These have the naming format
:code:`<prefix>__<backendLayoutName>` (with 2 underscores!).

For the provider in this extension the following locations and
conventions are used:

- backend layout:  **Resources/Private/BackendLayouts/**  ***<name>***
  **.ts**

- title:  **Resources/Private/Language/BackendLayouts.xlf** ; id =
  <name>

- icon:  **Resources/Public/Backend/Images/**  ***<name>***  **.**
  ***png\|gif***


.. toctree::
   :maxdepth: 5
   :titlesonly:
   :glob:

   CreatingBackendLayouts/Index
   UsageInTyposcript/Index

