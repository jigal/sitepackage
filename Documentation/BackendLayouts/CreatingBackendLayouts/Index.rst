﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Creating backend layouts
------------------------

For someone whose not familiar with the syntax the wizard can still be
used. Simply create a backend layout record, use the wizard button
next to the field for the definition and create the structures, set
the column numbers and title as you like. Save en close the wizard.
Instead of saving the record you can now copy the layout configuration
and paste it into a file in the sitepackage.

Add the title to the language file, create a nice icon and that's it.


