﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)


Usage in TypoScript
-------------------

The identifier can easily be used to determine which page template to
use for the frontend rendering:

::

   1 = LOAD_REGISTER
   1 {
     pageLayout.cObject = TEXT
     pageLayout.cObject {
       data = levelfield:-1, backend_layout_next_level, slide
       override.field = backend_layout
       split {
         token = typo3coder__
         1.current = 1
         1.wrap = |
       }
     }
   }
   
   10 = FLUIDTEMPLATE
   10 {
     file = EXT:typo3coder/Resources/Private/Templates/Templates/Main.html
     format = html
     partialRootPath = EXT:typo3coder/Resources/Private/Templates/Partials
     layoutRootPath = EXT:typo3coder/Resources/Private/Templates/Layout
   
     variables {
       layout = TEXT
       layout.data = register:pageLayout
     }
   }

The LOAD\_REGISTER part fetches the field in the current page, if
necessary from pages further up in the rootline and takes into
consideration the backend\_layout\_next\_level setting. After that it
removes the prefix and the two underscores to end up with the name of
the layout.

In the Fluid template it's now easy to use the correct template, which
we simply give the same name as the backend layout:

.. code-block:: xml

   <f:render partial="{layout}" arguments="{_all}"/>

The arguments attribute forwards all variables to the partial.


