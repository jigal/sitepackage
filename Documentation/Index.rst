﻿

.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. ==================================================
.. DEFINE SOME TEXTROLES
.. --------------------------------------------------
.. role::   underline
.. role::   typoscript(code)
.. role::   ts(typoscript)
   :class:  typoscript
.. role::   php(code)

===========
Sitepackage
===========

:Author:
      Jigal van Hemert

:Email:
      jigal.van.hemert@typo3.org

:Created:
      2014-06-23 20:01:04

:Changed:
      2014-06-24 14:02:03

:License:
      This document is published under the Open Content License
      available from http://www.opencontent.org/opl.shtml

:Rendered:
      |today|

The content of this document is related to TYPO3,
a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

-------------------------------------
website configuration in an extension
-------------------------------------

The goal of this project is to store as much website configuration as
possible in an extension. This can be useful for small scale websites
which do not have sophisticated deployment set-ups, but it's also
useful for larger installations because the configuration is now
stored in files which can be managed by versioning systems.

A big advantage of having (almost) all configuration in an extension
is that it's very easy to update existing installations with virtually
zero downtime. Even if there is only access to the TYPO3 backend
simply uploading a new version in the Extension Manager will activate
all the new configuration.


.. toctree::
   :maxdepth: 5
   :titlesonly:
   :glob:

   StructureAndTechniques/Index
   BaseExtension/Index
   Tsconfig/Index
   Typoscript/Index
   BackendLayouts/Index
   CustomContentElements/Index
   References/Index

