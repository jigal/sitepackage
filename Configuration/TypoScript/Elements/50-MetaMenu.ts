# Not used yet
lib.metaMenu = COA
lib.metaMenu  {
	10 = HMENU
	10 {
		special = list
		special.value = 11, 12
		includeNotInMenu = 1

		1 = TMENU
		1 {
			noBlur = 1
			wrap = <ul>|</ul>

			NO = 1
			NO {
				wrapItemAndSub = <li>|</li>
				ATagTitle.cObject = COA
				ATagTitle.cObject {
					10 = TEXT
					10.field = title
					10.noTrimWrap = || - |

					20 = TEXT
					20.field = subtitle
				}
			}

			CUR < .NO
			CUR {
				wrapItemAndSub = <li class="active">|</li>
			}
		}
	}

	30 = TEXT
	30.value = </map>
}
