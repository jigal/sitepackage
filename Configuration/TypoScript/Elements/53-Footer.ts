lib.social = COA
lib.social {
	10 = TEXT
	10 {
		value = &nbsp;
		typolink {
			parameter = https://www.facebook.com/jigal.vanhemert
			ATagParams = class="social-icon facebook"
		}
		wrap = <li>|</li>
	}
	20 = TEXT
	20 {
		value = &nbsp;
		typolink {
			parameter = https://twitter.com/jigalvh
			ATagParams = class="social-icon twitter"
		}
		wrap = <li>|</li>
	}
	30 = TEXT
	30 {
		value = &nbsp;
		typolink {
			parameter = http://nl.linkedin.com/in/jigalvh/
			ATagParams = class="social-icon linkedin"
		}
		wrap = <li>|</li>
	}
	40 = TEXT
	40 {
		value = &nbsp;
		typolink {
			parameter = http://forge.typo3.org/users/662
			ATagParams = class="social-icon typo3"
		}
		wrap = <li>|</li>
	}
}

lib.newsFooter = USER
lib.newsFooter {
	userFunc = tx_extbase_core_bootstrap->run
	extensionName = News
	pluginName = Pi1

	switchableControllerActions {
		News {
			1 = list
		}
	}

	settings < plugin.tx_news.settings
	settings {
		//categories = 49
		limit = 5
		detailPid = 27
		overrideFlexformSettingsIfEmpty := addToList(detailPid)
		startingpoint = 26
		templateLayout = 2
		link {
			skipControllerAndAction = 1
		}
	}
}

