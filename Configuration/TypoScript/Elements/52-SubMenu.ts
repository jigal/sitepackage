# Not used yet
lib.subMenu = HMENU
lib.subMenu {
	entryLevel = 1

	1 = TMENU
	1 {
		noBlur = 1
		wrap = <ul>|</ul>

		NO = 0
		NO.doNotShowLink = 1

		ACT = 1
		ACT {
			wrapItemAndSub = <li>|</li>
			ATagTitle.cObject = COA
			ATagTitle.cObject {
				10 = TEXT
				10.field = title
				10.noTrimWrap = || - |

				20 = TEXT
				20.field = subtitle
			}
		}

		CUR = 1
		CUR < .ACT
		CUR.wrapItemAndSub = <li class="active">|</li>
	}

	2 < .1
	2.NO >
	2.NO < .1.ACT

	3 < .2

	4 < .2

	5 < .2

	6 < .2

}

[PIDinRootline = 5]
lib.sectionNavigation = HMENU
lib.sectionNavigation {
	special = browse
	special {
		items = prev|next
		items.prevnextToSection = 1
	}
	1 = TMENU
	1 {
		NO = 1
		NO.wrapItemAndSub = <li class="left">|</li> || <li class="right">|</li>
		NO.stdWrap.wrap = &larr;| || |&rarr;
	}
}
[global]