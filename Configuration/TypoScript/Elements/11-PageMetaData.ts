page {

	meta.date.field		= tstamp
	meta.date.date		= Y-m-d\TH:m:sO
	meta.publisher		= Jigal van Hemert
	meta.page-topic		= typo3coder
	meta.audience		= all
	meta.author			= Jigal van Hemert
	meta.description	= typo3coder
	meta.description.override.field = description
	meta.keywords		=
	meta.keywords.override.field = keywords
	meta.robots			= follow,index,all
	meta.viewport       = width=device-width, initial-scale=1.0
}