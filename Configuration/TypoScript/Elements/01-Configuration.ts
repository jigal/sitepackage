config {
	doctype = html5
	xmlprologue = none
	disableCharsetHeader=0

	prefixComments = 0
	disablePrefixComment=1

	noPageTitle = 2
	disableImgBorderAttr = 1

	sys_language_mode = strict
	sys_language_overlay = hideNonTranslated
	sys_language_uid = 0
	language = en
	locale_all = en_US
	htmlTag_langKey = en
	htmlTag_dir = ltr

	removeDefaultJS = external
	inlineStyle2TempFile = 1
    compressJs = 1
    compressCss = 1
    concatenateJs = 1
    concatenateCss = 1

	prefixLocalAnchors = all
	absRefPrefix = auto
	tx_realurl_enable = 1
	uniqueLinkVars = 1
	extTarget = _blank
	intTarget >
	simulateStaticDocuments = 0
	typolinkCheckRootline = 1
	linkAcrossDomains = 1
	meaningfulTempFilePrefix = 100
	jumpurl_enable = 0
	jumpurl_mailto_disable = 0
	disablePageExternalUrl = 0

	spamProtectEmailAddresses = 4
	spamProtectEmailAddresses_atSubst = [at]

	noScaleUp = 0
	no_cache = 0
	cache_period = 604800
	cache_clearAtMidnight = 0
	sendCacheHeaders = 1
	enableContentLengthHeader = 1
	
		
	message_preview (
<div style="color:red;font-weight:bold;position:fixed;top:15px;left:15px;background-color:#cdcdcd;border:1px solid #aaa; padding:8px;box-shadow:0px 0px 5px #aaa;width:200px;line-height:50px;text-align:center;">PREVIEW!</div>
)
	admPanel = 0
	
	index_enable = 1
	index_metatags = 1
	index_externals = 1

	headerComment (
	Website developed by Jigal van Hemert
)
}

tt_content.image.20.maxW = 1200

# Add class to headings to match template class
lib.stdheader.3.headerClass.cObject.30 = TEXT
lib.stdheader.3.headerClass.cObject.30 {
    value = title-bg
    noTrimWrap = | ||
}
