lib.mainMenu = HMENU
lib.mainMenu {
	entryLevel = 0

	1 = TMENU
	1 {
		noBlur = 1
		expAll = 1
		wrap = <ul class="nav">|</ul>
		limit = 6

		NO = 1
		NO {
			wrapItemAndSub = <li>|</li>
			ATagTitle.cObject = COA
			ATagTitle.cObject {
				10 = TEXT
				10.field = title
				10.noTrimWrap = || - |
				
				20 = TEXT
				20.field = subtitle
			}
			doNotLinkIt = 0
		}

		ACT = 1
		ACT < .NO
		ACT.wrapItemAndSub = <li class="active">|</li>

		CUR = 1
		CUR < .ACT

		IFSUB = 1
		IFSUB < .NO
		IFSUB {
		    wrapItemAndSub = <li class="dropdown">|</li>
		    ATagParams = class="dropdown-toggle" data-toggle="dropdown"
		    stdWrap.wrap = |<b class="caret"></b>
		    doNotLinkIt = 0

		}

		ACTIFSUB = 1
		ACTIFSUB < .IFSUB
		ACTIFSUB {
		    wrapItemAndSub = <li class="dropdown active">|</li>
		}

		CURIFSUB = 1
		CURIFSUB < .ACTIFSUB
	}

	2 < .1
	2.limit >
	2.wrap = <ul class="dropdown-menu">|</ul>

	3 < .2
	3.wrap = <ul class="dropdown-submenu">|</ul>

}
lib.mainMenuMobile = HMENU
lib.mainMenuMobile {
    entryLevel = 0

    1 = TMENU
    1 {
        noBlur = 1
        expAll = 1
        wrap = <form action="#" id="mobile-nav" class="visible-phone"><div class="mobile-nav-select"><select onchange="window.open(this.options[this.selectedIndex].value,'_top')"><option value="">Navigate...</option>|</select></div></form>
        NO = 1
        NO {
            doNotLinkIt = 1
            allWrap.cObject = TEXT
            allWrap.cObject {
                value {
                    typolink {
                        parameter.field = uid
                        returnLast = url
                    }
                    wrap = <option value="#">|</option>
                    wrap.splitChar = #
                }
            }
        }
    }

    2 < .1
    2.NO.allWrap.cObject.value.wrap = <option value="#">- |</option>
    2.wrap >
    3 < .1
    3.NO.allWrap.cObject.value.wrap = <option value="#">-- |</option>
    3.wrap >
}
lib.mainMenuFooter = HMENU
lib.mainMenuFooter {
	entryLevel = 0
	1 = TMENU
	1 {
		expAll = 1
		limit = 6
		NO = 1
		NO {
			wrapItemAndSub = | || &nbsp;&nbsp;&nbsp;&#124;&nbsp;&nbsp;&nbsp;|
		}
	}
}