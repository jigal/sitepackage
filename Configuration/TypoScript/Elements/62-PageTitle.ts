lib.pageTitle = COA
lib.pageTitle {
	10 = TEXT
	10 {
		data = page:title
	}
}
lib.subTitle = COA
lib.subTitle {
	20 = TEXT
	20 {
		data = page:subtitle
	}
}
lib.siteName = TEXT
lib.siteName.value = TYPO3coder

lib.siteByLine = TEXT
lib.siteByLine.value = TYPO3 development for fun