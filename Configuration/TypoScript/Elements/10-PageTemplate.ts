page = PAGE
page {
	typeNum = 0

	includeCSSLibs {
		googleFont = http://fonts.googleapis.com/css?family=Noto+Sans:400,700italic,400italic,700&subset=latin,greek-ext,greek,latin-ext
		googleFont {
			excludeFromConcatenation = 1
			external = 1
		}
		compatibility = EXT:typo3coder/Resources/Public/StyleSheets/style-ie.css
		compatibility {
		    media = screen,projection
		    allWrap = <!--[if lt IE 9]>|<![endif]-->
		}
	}

    includeCSS {
        01bootstrap = EXT:typo3coder/Resources/Public/StyleSheets/bootstrap.css
        02bootstrapresponsive = EXT:typo3coder/Resources/Public/StyleSheets/bootstrap-responsive.css
        03prettyPhoto = EXT:typo3coder/Resources/Public/StyleSheets/prettyPhoto.css
        04flexslider = EXT:typo3coder/Resources/Public/StyleSheets/flexslider.css
        05customStyles = EXT:typo3coder/Resources/Public/StyleSheets/custom-styles.css
    }

	includeJSlibs {
		html5shiv = //html5shiv.googlecode.com/svn/trunk/html5.js
		html5shiv {
			allWrap = <!--[if lt IE 9]>|<![endif]-->
			external = 1
			excludeFromConcatenation = 1
			disableCompression = 1 
		}
		prettycode = https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js
		prettycode {
		    external = 1
		    excludeFromConcatenation = 1
		    disableCompression = 1
		}
		bootstrap = EXT:typo3coder/Resources/Private/JavaScript/bootstrap.js
		prettyPhoto = EXT:typo3coder/Resources/Private/JavaScript/jquery.prettyPhoto.js
		flexslider = EXT:typo3coder/Resources/Private/JavaScript/jquery.flexslider.js
		jqueryCustom = EXT:typo3coder/Resources/Private/JavaScript/jquery.custom.js
	}

	jsInline.10 = TEXT
	jsInline.10.value (
        $(document).ready(function () {

            $("#btn-blog-next").click(function () {
              $('#blogCarousel').carousel('next')
            });
             $("#btn-blog-prev").click(function () {
              $('#blogCarousel').carousel('prev')
            });

             $("#btn-client-next").click(function () {
              $('#clientCarousel').carousel('next')
            });
             $("#btn-client-prev").click(function () {
              $('#clientCarousel').carousel('prev')
            });

        });

         $(window).load(function(){

            $('.flexslider').flexslider({
                animation: "slide",
                slideshow: true,
                start: function(slider){
                  $('body').removeClass('loading');
                }
            });
        });
	)

	includeJSFooterlibs {
	}

	includeJSFooter {
	}

    javascriptLibs {
        jQuery = 1
        jQuery {
            version = 1.8.3
            source = jquery
            noConflict = 0
        }
    }

	headerData.10 =< lib.pageTitle
	headerData.10.wrap = <title>|</title>

	bodyTagCObject = TEXT
	bodyTagCObject {
		wrap = <body class="|">
		data = register:pageLayout
	}

	1 = LOAD_REGISTER
	1 {
		pageLayout.cObject = TEXT
		pageLayout.cObject {
            data = pagelayout
            split {
                token = pagets__
                1.current = 1
                1.wrap = |
            }
		}
	}

	10 = FLUIDTEMPLATE
	10 {
		templateName = Main
		format = html
		partialRootPaths {
			10 = EXT:typo3coder/Resources/Private/Templates/Partials
		}
		layoutRootPaths {
			10 = EXT:typo3coder/Resources/Private/Templates/Layout
		}
		templateRootPaths {
			10 = EXT:typo3coder/Resources/Private/Templates/Templates
		}

		variables {
			layout = TEXT
			layout.data = register:pageLayout
			layout.replacement {
				10 {
					search.char = 32
					replace =
				}
			}
		}
	}
}

lib.content0 < styles.content.get
lib.content1 < styles.content.getLeft
lib.content2 < styles.content.getRight
lib.content3 < styles.content.getBorder
lib.content4 < styles.content.get
lib.content4.select.where = colPos=4
lib.content5 < styles.content.get
lib.content5.select.where = colPos=5