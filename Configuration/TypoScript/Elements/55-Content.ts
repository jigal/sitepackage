##########################
# INTRO content element
##########################
tt_content.intro = FLUIDTEMPLATE
tt_content.intro {
    template = FILE
    template.file = EXT:typo3coder/Resources/Private/Templates/ContentELements/Intro.html
    layoutRootPath = EXT:typo3coder/Resources/Private/Templates/Layout/
	dataProcessing {
		10 = jigal\typo3coder\DataProcessing\FlexFormProcessor
		10 {
			as = flexformData
		}
	}
    variables {
    }
}
##########################
# SLIDER content element
##########################
tt_content.slider = FLUIDTEMPLATE
tt_content.slider {
    template = FILE
    template.file = EXT:typo3coder/Resources/Private/Templates/ContentELements/Slider.html
    layoutRootPath = EXT:typo3coder/Resources/Private/Templates/Layout/
	dataProcessing {
		10 = jigal\typo3coder\DataProcessing\FlexFormProcessor
		10 {
			as = flexformData
		}
	}
    variables {
    }
}
##########################
# TER SLIDER content element
##########################
tt_content.terslider = FLUIDTEMPLATE
tt_content.terslider {
    template = FILE
    template.file = EXT:typo3coder/Resources/Private/Templates/ContentELements/Terslider.html
    layoutRootPath = EXT:typo3coder/Resources/Private/Templates/Layout/
	dataProcessing {
		10 = jigal\typo3coder\DataProcessing\FlexFormProcessor
		10 {
			as = flexformData
		}
	}
    variables {
    }
}

##########################
# frames to set width of
# content element
##########################
tt_content.stdWrap.innerWrap.cObject {
    100 = TEXT
    100.value = <div class="span1">|</div>
    110 = TEXT
    110.value = <div class="span2">|</div>
    120 = TEXT
    120.value = <div class="span3">|</div>
    130 = TEXT
    130.value = <div class="span4">|</div>
    140 = TEXT
    140.value = <div class="span5">|</div>
    150 = TEXT
    150.value = <div class="span6">|</div>
    160 = TEXT
    160.value = <div class="span7">|</div>
    170 = TEXT
    170.value = <div class="span8">|</div>
    180 = TEXT
    180.value = <div class="span9">|</div>
    190 = TEXT
    190.value = <div class="span10">|</div>
    200 = TEXT
    200.value = <div class="span11">|</div>
    210 = TEXT
    210.value = <div class="span12">|</div>
    # No frame
    220 = TEXT
    220.value = |
    # Use full width as default
    default < .210
}

########################
# default FORM object
########################
tt_content.mailform.20 {
	layout {
		containerWrap (
			<div class="formcontainer"><elements /></div>
		)
		elementWrap (
			<div class="row"><element /></div>
		)
		textline (
			<label /><input />
		)
		submit (
			<div class="span2"><label /><input /></div>
		)
	}
}

tt_content.image.20.1.imageLinkWrap {
	JSwindow = 0
	directImageLink = 1
	linkParams.ATagParams.dataWrap = rel="prettyPhoto"
}