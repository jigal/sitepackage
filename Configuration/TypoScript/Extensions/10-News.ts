# News configuration
plugin.tx_news {
	view {
		templateRootPaths {
			200 = EXT:typo3coder/Resources/Private/Templates/Extensions/news/Templates/
		}
		partialRootPaths {
			200 = EXT:typo3coder/Resources/Private/Templates/Extensions/news/Partials/
		}
		layoutRootPaths {
			200 = EXT:typo3coder/Resources/Private/Templates/Extensions/news/Layouts/
		}
	}
	settings {
		defaultDetailPid = 27
		backPid >
		detail {
			showSocialShareButtons = 0
			media.image.lightbox = 0
			media.image.class = img-thumbnail
		}
		link {
			skipControllerAndAction = 1
		}
	}
	_LOCAL_LANG.default {
		author = %1s

	}
}