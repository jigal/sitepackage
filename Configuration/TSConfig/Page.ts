# include all the backend layouts
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:typo3coder/Resources/Private/BackendLayouts" extensions="ts">

TCEFORM.tt_content {
    # disable a few fields
	table_bgColor.disabled = 1
	table_border.disabled = 1
	table_cellspacing.disabled = 1
	table_cellpadding.disabled = 1
	linkToTop.disabled = 1
	# remove default frames
	section_frame {
		removeItems = 1,2,5,6,10,11,12,20,21,66
		altLabels {
		}
	}
	image_frames.disabled=1

	header_layout {
		removeItems = 0,1,5,6,7
		altLabels {
			2 = Page overview
			3 = Heading, level 2
			4 = Heading, level 3
        }
	}
	image.config.disable_controls=upload
}

TCEMAIN {
    # extra cache handling settings
	clearCache_pageGrandParent=1
	clearCache_pageSiblingChildren=1

    # permissions and user/group IDs for new pages
	permissions {
		userid	= 1
		groupid	= 1
		user	= 31
		group	= 31
		everybody = 31
	}
}

TCAdefaults {
    # default backend layout for new pages
	pages {
		backend_layout = pagets__nosidebar
	}
}

# configuration for backend modules
mod {
	web_list {
		alternateBgColors = 1
	}

	web_layout {
		disableBigButtons = 1
	}

    # icon for default language
	SHARED {
		defaultLanguageFlag = gb
		defaultLanguageLabel = English
	}
	wizards.newContentElement.wizardItems.extra {
		header = LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra
		elements {
			intro {
				iconIdentifier = content-element-intro
				title = LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_intro_title
				description = LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_intro_description
				tt_content_defValues {
					CType = intro
				}
			}
			slider {
				iconIdentifier = content-element-slider
				title = LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_slider_title
				description = LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_slider_description
				tt_content_defValues {
					CType = slider
				}
			}
			terslider {
				iconIdentifier = conent-element-terslider
				title = LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_terslider_title
				description = LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_terslider_description
				tt_content_defValues {
					CType = terslider
				}
			}
		}
		show = *
	}
}

# RTE configuration
RTE.default {
	contentCSS = EXT:typo3coder/Resources/Public/Backend/StyleSheets/Fonts.css
	useCSS = 1
	showTagFreeClasses = 0
	classesTD >
	classesTable >

    buttons.blockstyle.tags.p.allowedClasses = title-bg
    buttons.blockstyle.tags.pre.allowedClasses = prettyprint
	disablePCexamples = 1
	classesImage >
	colors >
	disableColorPicker = 1
	disableSelectColor = 1
	hideFontFaces = *
	hideFontSizes = *

	showButtons := addToList(line,removeformat,center, right, justifyfull,softhyphen)

	keepButtonGroupTogether = 1
	hideTableOperationsInToolbar = 0
	keepToggleBordersInToolbar = 1
	showStatusBar = 1

	removeComments = 1
	removeTags := addToList(h1,h2, img)
	hidePStyleItems := addToList(h1,h2,section,aside,article,div,header,footer,nav)
	removeTagsAndContents := addToList(script)
	removeTrailingBR = 1

	disableSpacingFieldsetInTableOperations = 1
	disableColorFieldsetInTableOperations = 1
	disableLayoutFieldsetInTableOperations = 1
	disableBordersFieldsetInTableOperations = 1

	buttons.formatblock {
	    removeItems = address,h1,h2,section,aside,article,header,footer,nav
	}
	buttons.textstyle.items := addToList(code)

	proc {
		allowTagsOutside := addToList(ul,div,table,pre)
		allowTags = a, abbr, acronym, address, blockquote, br, cite, code, col, colgroup, dd,  dfn, dl, div, dt, em, h2, h3, h4,  hr , li, link,  ol, p, pre, strike, strong, sub, sup, table, thead, tbody, tfoot, td, th, tr, ul, iframe
		entryHTMLparser_db.allowTags < .allowTags
		allowedClasses =  external-link, external-link-new-window, internal-link, internal-link-new-window, download, mail, indent, teaser, title-bg, prettyprint
	}
}

RTE.config.tt_content.bodytext.proc.allowedClasses < RTE.default.proc.allowedClasses

RTE.classes {
    title-bg.name = Heading as divider
    prettyprint.name = Code highlighted
}

#frames for columns
# TODO: alternative for FSC
TCEFORM.tt_content.section_frame {
    addItems {
        100 = 1/12th
        110 = 1/6th
        120 = 1/4th
        130 = 1/3th
        140 = 5/12th
        150 = 1/2th
        160 = 7/12th
        170 = 2/3th
        180 = 3/4th
        190 = 5/6th
        200 = 11/12th
        210 = full width
        220 = No frame
    }
}

# News layouts
tx_news.templateLayouts {
	1 = LLL:EXT:typo3coder/Resources/Private/Language/News.xlf:newsSlider
	2 = LLL:EXT:typo3coder/Resources/Private/Language/News.xlf:newsFooterList
}