<?php
// intro CE
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array(
	'LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_intro_title',
	'intro',
	'EXT:typo3coder/Resources/Public/Backend/Images/intro_small.png'
);
$GLOBALS['TCA']['tt_content']['palettes']['introheader'] = array(
	'showitem' => 'header;LLL:EXT:cms/locallang_ttc.xlf:header_formlabel,
					--linebreak--, subheader;LLL:EXT:cms/locallang_ttc.xlf:subheader_formlabel,
					--linebreak--, header_link;LLL:EXT:cms/locallang_ttc.xlf:header_link_formlabel',
	'canNotCollapse' => 1
);
$GLOBALS['TCA']['tt_content']['types']['intro'] = array(
	'showitem' => '--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
					section_frame;LLL:EXT:cms/locallang_ttc.xlf:section_frame_formlabel,
					--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.header;introheader,
					bodytext;Text;;richtext:rte_transform[flag=rte_enabled|mode=ts_css],
					rte_enabled;LLL:EXT:cms/locallang_ttc.xlf:rte_enabled_formlabel,
				--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,
					--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.visibility;visibility,
					--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.access;access,
				--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.extended'
);

// slider CE
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array(
	'LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_slider_title',
	'slider',
	'EXT:typo3coder/Resources/Public/Backend/Images/slider_small.png'
);
// flexform for pi_flexform field
$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][',slider'] =
	'FILE:EXT:typo3coder/Configuration/FlexForms/flexform_slider.xml';
$GLOBALS['TCA']['tt_content']['types']['slider'] = array(
	'showitem' => '--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
					section_frame;LLL:EXT:cms/locallang_ttc.xlf:section_frame_formlabel,
					pi_flexform; ;,
				--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,
					--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.visibility;visibility,
					--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.access;access,
				--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.extended'
);

// terslider CE
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array(
	'LLL:EXT:typo3coder/Resources/Private/Language/newContentElements.xlf:extra_terslider_title',
	'terslider',
	'EXT:typo3coder/Resources/Public/Backend/Images/terslider_small.png'
);
// flexform for pi_flexform field
$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][',terslider'] =
	'FILE:EXT:typo3coder/Configuration/FlexForms/flexform_terslider.xml';
$GLOBALS['TCA']['tt_content']['types']['terslider'] = array(
	'showitem' => '--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
					section_frame;LLL:EXT:cms/locallang_ttc.xlf:section_frame_formlabel,
					pi_flexform; ;,
				--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,
					--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.visibility;visibility,
					--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.access;access,
				--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.extended'
);