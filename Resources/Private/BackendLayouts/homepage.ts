mod {
	web_layout {
		BackendLayouts {
			homepage {
				title = LLL:EXT:typo3coder/Resources/Private/Language/BackendLayouts.xlf:homepage
				icon = EXT:typo3coder/Resources/Public/Backend/Images/homepage.png
				config {
					backend_layout {
						colCount = 2
						rowCount = 3
						rows {
							1 {
								columns {
									1 {
										name = Slider
										colPos = 1
									}
									2 {
										name = Intro
										colPos = 2
									}
								}
							}
							2 {
								columns {
									1 {
										name = Content
										colspan = 2
										colPos = 0
									}
								}
							}
							3 {
								columns {
									1 {
										name = News
										colPos = 4
									}
									2 {
										name = Extensions
										colPos = 5
									}
								}
							}
						}
					}
				}
			}
		}
	}
}