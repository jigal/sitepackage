mod {
	web_layout {
		BackendLayouts {
			rightsidebar {
				title = LLL:EXT:typo3coder/Resources/Private/Language/BackendLayouts.xlf:rightsidebar
				icon = EXT:typo3coder/Resources/Public/Backend/Images/rightsidebar.png
				config {
					backend_layout {
						colCount = 2
						rowCount = 1
						rows {
							1 {
								columns {
									1 {
										name = Content
										colPos = 0
									}
									2 {
										name = Right
										colPos = 2
									}
								}
							}
						}
					}
				}
			}
		}
	}
}