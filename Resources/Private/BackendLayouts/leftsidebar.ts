mod {
	web_layout {
		BackendLayouts {
			leftsidebar {
				title = LLL:EXT:typo3coder/Resources/Private/Language/BackendLayouts.xlf:leftsidebar
				icon = EXT:typo3coder/Resources/Public/Backend/Images/leftsidebar.png
				config {
					backend_layout {
						colCount = 2
						rowCount = 1
						rows {
							1 {
								columns {
									1 {
										name = Left
										colPos = 1
									}
									2 {
										name = Content
										colPos = 0
									}
								}
							}
						}
					}
				}
			}
		}
	}
}