mod {
	web_layout {
		BackendLayouts {
			nosidebar {
				title = LLL:EXT:typo3coder/Resources/Private/Language/BackendLayouts.xlf:nosidebar
				icon = EXT:typo3coder/Resources/Public/Backend/Images/nosidebar.png
				config {
					backend_layout {
						colCount = 1
						rowCount = 1
						rows {
							1 {
								columns {
									1 {
										name = Content
										colPos = 0
									}
								}
							}
						}
					}
				}
			}
		}
	}
}