<?php
$EM_CONF[$_EXTKEY] = array(
	'title' => 'typo3coder sitepackage',
	'description' => 'TypoScript, HTML/CSS and JavaScript plus specific configuration for typo3coder.',
	'category' => 'plugin',
	'author' => 'Jigal van Hemert',
	'author_email' => 'jigal.van.hemert@typo3.org',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author_company' => '',
	'version' => '1.0.0',
	'constraints' => array(
		'depends' => array(
			'typo3' => '6.2.0'
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:21:{s:9:"ChangeLog";s:4:"334e";s:10:"README.txt";s:4:"ee2d";s:12:"ext_icon.gif";s:4:"95dd";s:17:"ext_localconf.php";s:4:"eccb";s:14:"ext_tables.php";s:4:"173e";s:14:"ext_tables.sql";s:4:"27ce";s:31:"icon_tx_rswinelist_category.gif";s:4:"8fd7";s:35:"icon_tx_rswinelist_wineproducts.gif";s:4:"8583";s:13:"locallang.xml";s:4:"d309";s:16:"locallang_db.xml";s:4:"f50a";s:7:"tca.php";s:4:"5a6d";s:19:"doc/wizard_form.dat";s:4:"aa95";s:20:"doc/wizard_form.html";s:4:"99af";s:14:"pi1/ce_wiz.gif";s:4:"02b6";s:31:"pi1/class.tx_rswinelist_pi1.php";s:4:"9176";s:39:"pi1/class.tx_rswinelist_pi1_wizicon.php";s:4:"f608";s:13:"pi1/clear.gif";s:4:"cc11";s:13:"pi1/close.gif";s:4:"8e04";s:17:"pi1/locallang.xml";s:4:"71b9";s:24:"pi1/static/editorcfg.txt";s:4:"7fcd";s:20:"pi1/static/setup.txt";s:4:"1ad7";}',
	'suggests' => array(
	),
);

?>