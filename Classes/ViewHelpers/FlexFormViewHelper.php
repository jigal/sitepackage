<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Jigal van Hemert <jigal.van.hemert@typo3.org>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace jigal\typo3coder\ViewHelpers;

class FlexFormViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @var bool
	 */
	protected $escapeOutput = false;

	/**
	 * Extracts data from flexform field of content record
	 *
	 * @return string
	 */
	public function render() {
		$data = $this->templateVariableContainer->get('data');

		// get data from pi_flexform field
		$flexForm = \TYPO3\CMS\Core\Utility\GeneralUtility::xml2array($data['pi_flexform']);
		$flexForm = $flexForm['data'];

		$data = array();
		if (is_array($flexForm)) {
			foreach ($flexForm as $tabName => $tabContent) {
				// get content from lDEF part of each tab
				$tabContent = $tabContent['lDEF'];
				$data[$tabName] = array();
				if (is_array($tabContent)) {
					foreach ($tabContent as $field => $value) {
						$data[$tabName][$field] = $this->getValueFromFlexFormContent($value);
					}
				}
			}
		}
		// put result in variable "flexform"
		$this->templateVariableContainer->add('flexform', $data);
		unset($flexForm);
		return $this->renderChildren();
	}

	/**
	 * Gets values of parts of the flexform content
	 *
	 * @param array $part Parts to retrieve
	 * @return array|string
	 */
	protected function getValueFromFlexFormContent(array $part) {
		$result = '';
		if (array_key_exists('vDEF', $part)) {
			// simple value found
			$result = $part['vDEF'];
		} elseif (array_key_exists('el', $part) && is_array($part['el'])) {
			// subitems are handled by recursive calls
			$value = array();
			foreach ($part['el'] as $tag => $data) {
				if (array_key_exists('_TOGGLE', $data)) {
					$data = array_shift($data);
				}
				$value[$tag] = $this->getValueFromFlexFormContent($data);
			}
			$result = $value;
		}
		return $result;
	}
}