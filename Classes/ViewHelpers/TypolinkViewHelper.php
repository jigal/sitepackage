<?php
namespace jigal\typo3coder\ViewHelpers;

/*                                                                        *
 * This script is part of the TYPO3 project - inspiring people to share!  *
 *                                                                        *
 * TYPO3 is free software; you can redistribute it and/or modify it under *
 * the terms of the GNU General Public License version 2 as published by  *
 * the Free Software Foundation.                                          *
 *                                                                        *
 * This script is distributed in the hope that it will be useful, but     *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHAN-    *
 * TABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General      *
 * Public License for more details.                                       *
 *                                                                        */
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * A view helper to create links from fields supported by the link wizard
 *
 * == Example ==
 *
 * {link} contains "19 _blank - "testtitle with whitespace" &X=y"
 *
 * <code title="minimal usage">
 * <f:link.typolink parameter="{link}">
 * Linktext
 * </f:link.typolink>
 * <output>
 * <a href="index.php?id=19&X=y" title="testtitle with whitespace" target="_blank">
 * some content
 * </a>
 * </output>
 *
 * </code>
 * <code title="Full parameter usage">
 * <f:link.typolink parameter="{link}" target="_blank" class="ico-class" title="some title">
 * Linktext
 * </f:link.typolink>
 * </code>
 * <output>
 * <a href="index.php?id=19&X=y" title="some title" target="_blank" class="ico-class">
 * some content
 * </a>
 * </output>
 *
 *
 */
class TypolinkViewHelper extends AbstractViewHelper {

	/**
	 * @var bool
	 */
	protected $escapeOutput = false;

	/**
	 * @param string $parameter stdWrap.typolink style parameter string
	 * @param string $target
	 * @param string $class
	 * @param string $title
	 * @param string $additionalParams
	 *
	 * @return string
	 */
	public function render($parameter, $target = '', $class = '', $title = '', $additionalParams = '') {
		$typolink = $this->constructTypolinkConfiguration($parameter, $target, $class, $title, $additionalParams);

		$parameter = implode(' ', $typolink);

		$content = $this->renderChildren();
		if ($parameter) {
			/** @var ContentObjectRenderer $contentObject */
			$contentObject = GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\ContentObject\\ContentObjectRenderer');
			$contentObject->start(array(), '');
			$content = $contentObject->stdWrap($content, array(
				'typolink.' => array(
					'parameter' => $parameter
				)
			));
		}
		return $content;
	}

	/**
	 * @param string $parameter
	 * @param string $target
	 * @param string $class
	 * @param string $title
	 * @param string $additionalParams
	 *
	 * @return array
	 */
	protected function constructTypolinkConfiguration($parameter, $target = '', $class = '', $title = '',
		$additionalParams = '') {
		// typolink from wizard might be linkparam target class title (divided by space)
		$parameterConfiguration = GeneralUtility::unQuoteFilenames($parameter, TRUE);

		if (empty($parameterConfiguration)) {
			return $parameterConfiguration;
		}

		$typolinkConfiguration = array($parameterConfiguration[0]);

		// target gets overridden
		$typolinkConfiguration[1] = '';
		if ($target) {
			$typolinkConfiguration[1] = $target;
		} elseif (isset($parameterConfiguration[1])) {
			$typolinkConfiguration[1] = $parameterConfiguration[1];
		}

		// class gets concatenated in case it is a real class (- is the empty value and will be removed)
		$typolinkConfiguration[2] = '';
		if (isset($parameterConfiguration[2])) {
			$typolinkConfiguration[2] = $parameterConfiguration[2];
		}
		if ($class) {
			$typolinkConfiguration[2] = $typolinkConfiguration[2] !== '-' ? $typolinkConfiguration[2] . ' ' : '';
			$typolinkConfiguration[2] .= $class;
		}

		// title gets overridden
		$typolinkConfiguration[3] = '';
		if (isset($parameterConfiguration[3])) {
			$typolinkConfiguration[3] = $parameterConfiguration[3];
		}
		if ($title) {
			$typolinkConfiguration[3] = $title;
		}

		// additionalParams gets concatenated
		$typolinkConfiguration[4] = '';
		if (isset($parameterConfiguration[4])) {
			$typolinkConfiguration[4] = $parameterConfiguration[4];
		}
		if ($additionalParams) {
			$typolinkConfiguration[4] .= $additionalParams;
		}

		$lastWithValue = -1;
		for ($i = 4; $i >= 0; $i--) {
			if ($typolinkConfiguration[$i] === '') {
				// check if there's something behind this element
				if ($lastWithValue > $i) {
					$typolinkConfiguration[$i] = '-';
				} else {
					unset($typolinkConfiguration[$i]);
				}
			} else {
				$lastWithValue = $i;
				if ($typolinkConfiguration[$i] !== '-') {
					$typolinkConfiguration[$i] = '"' . $typolinkConfiguration[$i] . '"';
				}
			}
		}

		return $typolinkConfiguration;
	}
}