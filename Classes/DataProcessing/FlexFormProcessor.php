<?php
namespace jigal\typo3coder\DataProcessing;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\FlexFormService;

/**
 * Class for flexform processing for a content element
 *
 * Example TypoScript configuration:
 *
 * dataProcessing {
 *   10 = VENDOR\MyExtension\DataProcessing\FlexFormProcessor
 * }
 *
 */
class FlexFormProcessor implements DataProcessorInterface
{

    /**
     * Process flexform data
     *
     * @param ContentObjectRenderer $cObj The data of the content element or page
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     * @return array the processed data as key/value store
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $flexFormService = GeneralUtility::makeInstance(FlexFormService::class);

        // The variable to be used within the result
        $targetVariableName = $cObj->stdWrapValue('as', $processorConfiguration, 'flexFormData');

        $processedData[$targetVariableName] =
            $flexFormService->convertFlexFormContentToArray($processedData['data']['pi_flexform']);

        return $processedData;
    }
}