<?php
defined('TYPO3_MODE') or die();

use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
//include_once(ExtensionManagementUtility::extPath($_EXTKEY) . '/Configuration/PHP/realurl.php');

if (TYPO3_MODE === 'BE') {

	$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'content-element-intro',
        \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
        array(
            'source' => 'EXT:typo3coder/Resources/Public/Backend/Images/intro.png'
        )
    );
    $iconRegistry->registerIcon(
        'content-element-slider',
        \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
        array(
            'source' => 'EXT:typo3coder/Resources/Public/Backend/Images/slider.png'
        )
    );
    $iconRegistry->registerIcon(
        'content-element-terslider',
        \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
        array(
            'source' => 'EXT:typo3coder/Resources/Public/Backend/Images/terslider.png'
        )
    );
}